package sandbox;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class TestXML {

    public static void main(String[] args) throws Exception {
        TestXML testXML = new TestXML();
        Document document = testXML.parse();
        List<Node> list = document.selectNodes("//TestCases/Function/TestCase");
        list.forEach((testcase) -> {
            System.out.println("NAME: "+testcase.getName());
            System.out.println(testcase.valueOf("@id"));
            List<Node> varList = testcase.selectNodes("Input/Var");
            varList.forEach((var) -> {
                System.out.println(var.valueOf("@name"));
                System.out.println(var.valueOf("@value"));
                List<Node> hasList = var.selectNodes("Has");
                hasList.forEach((has) -> {
                    System.out.println(has.valueOf("@name"));
                    System.out.println(has.valueOf("@value"));
                });
            });
        });
    }

    public Document parse() throws DocumentException, MalformedURLException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL url = new File(classLoader.getResource("tcases/notepad-options-Test.xml").getFile()).toURI().toURL();
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }
}
