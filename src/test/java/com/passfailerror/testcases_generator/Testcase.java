package com.passfailerror.testcases_generator;

import java.util.ArrayList;
import java.util.List;

public class Testcase {

    String name;
    Integer id;
    List<Variable> variableList = new ArrayList<>();

    public Testcase(){};

    public void setName(String name){this.name = name;}
    public void setId(Integer id){this.id = id;}
    public void addVariable(Variable variable){this.variableList.add(variable);}

    public List<Variable> getVariableList(){return variableList;}

    public Integer getId() {
        return id;
    }
}
