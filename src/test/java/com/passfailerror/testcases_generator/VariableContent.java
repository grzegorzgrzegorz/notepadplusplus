package com.passfailerror.testcases_generator;

public enum VariableContent {

    GHERKIN_TYPE("gherkinType"),
    EXPECTED("expected"),
    COMMAND("command");


    String item;

    private VariableContent(String item){
        this.item = item;
    }

    public String getItem() {
        return item;
    }
}
