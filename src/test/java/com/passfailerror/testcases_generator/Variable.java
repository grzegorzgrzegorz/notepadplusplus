package com.passfailerror.testcases_generator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.passfailerror.testcases_generator.VariableContent.*;

public class Variable {

    String name;
    String value;
    List<HasElement> hasElementList = new ArrayList<>();

    public Variable(){}

    public void setName(String name){this.name = name;}
    public void setValue(String value){this.value = value;}
    public void addHasElement(HasElement hasElement){this.hasElementList.add(hasElement);}

    public List<HasElement> getHasElementList(){return hasElementList;}

    public String getValue(){
        return value;
    }

    public String getGherkinValue(){
        return getVariableContentByName(GHERKIN_TYPE);
    }
    public String getCommandValue(){
        return getVariableContentByName(COMMAND);
    }
    public String getExpectedValue(){
        return getVariableContentByName(EXPECTED);
    }

    private String getVariableContentByName(VariableContent variableContent){
        try {
            return hasElementList.stream().filter(hasElement -> hasElement.getName().contentEquals(variableContent.getItem())).findFirst().get().getValue();
        }
        catch (NoSuchElementException ex){
            return "";
        }
    }

}
