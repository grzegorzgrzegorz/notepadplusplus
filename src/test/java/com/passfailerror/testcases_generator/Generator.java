package com.passfailerror.testcases_generator;

import com.passfailerror.notepadplusplus.enums.StatusBarControlPatterns;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import sandbox.TestXML;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Generator {

    public static String TEARDOWN = "TEARDOWN().tab().isClosed();";

    Document document;
    List<Testcase> testcaseList = new ArrayList<>();

    Logger my_logger = LogManager.getLogger(this.getClass().getName());

    public Generator(String path) throws MalformedURLException, DocumentException {

        this.document = parse(path);
        TestXML testXML = new TestXML();
        Document document = testXML.parse();
        List<Node> list = document.selectNodes("//TestCases/Function/TestCase");
        list.forEach((testcase) -> {
            Testcase currentTestcase = new Testcase();
            currentTestcase.setName(testcase.getName());
            currentTestcase.setId(Integer.valueOf(testcase.valueOf("@id")));
            List<Node> varList = testcase.selectNodes("Input/Var");
            varList.forEach((var) -> {
                Variable currentVariable = new Variable();
                currentVariable.setName(var.valueOf("@name"));
                currentVariable.setValue(var.valueOf("@value"));
                List<Node> hasList = var.selectNodes("Has");
                hasList.forEach((has) -> {
                    HasElement currentHas = new HasElement();
                    currentHas.setName(has.valueOf("@name"));
                    currentHas.setValue(has.valueOf("@value"));
                    currentVariable.addHasElement(currentHas);
                });
                currentTestcase.addVariable(currentVariable);
            });
            testcaseList.add(currentTestcase);
        });
    }


    public Document parse(String path) throws DocumentException, MalformedURLException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL url = new File(classLoader.getResource(path).getFile()).toURI().toURL();
        SAXReader reader = new SAXReader();
        return reader.read(url);
    }

    public void go(String path) throws IOException, URISyntaxException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL url = new URL(classLoader.getResource("").toString()+path);
        FileWriter fileWriter = new FileWriter(Paths.get(url.toURI()).toFile());
        fileWriter.write(generateDsl());
        fileWriter.close();
    }

    public String generateDsl(){
           StringBuilder generatedTestcases = new StringBuilder();
            testcaseList.forEach(testcase ->{
                my_logger.info("processing testcase "+testcase.getId().toString());
                StringBuilder currentTestcase = new StringBuilder();
                currentTestcase.append("@Test").append(System.getProperty("line.separator"));
                currentTestcase.append("public void testNewDocumentPreferences_tc").append(testcase.getId().toString()).append("(){");
                currentTestcase.append(System.getProperty("line.separator"));
                testcase.getVariableList().forEach(variable ->{
                    currentTestcase.append(variable.getGherkinValue()).append("(").append(")");
                    currentTestcase.append(".");
                    currentTestcase.append(processSyntaxPlaceholders(variable.getCommandValue()));
                    currentTestcase.append("(").append(processVarValuePlaceholders(variable.getValue())).append(")").append(".");
                    currentTestcase.append(System.getProperty("line.separator"));
                });
                testcase.getVariableList().forEach(variable -> {
                    if (!variable.getExpectedValue().isEmpty()) {
                        currentTestcase.append("THEN").append("(").append(")").append(".");
                        currentTestcase.append(processSyntaxPlaceholders(processExpectedValuePlaceHolders(variable.getExpectedValue())));
                        currentTestcase.append(".");
                        currentTestcase.append(System.getProperty("line.separator"));
                    }
                });
                currentTestcase.replace(currentTestcase.length()-3,currentTestcase.length()-2, ";");
                currentTestcase.append(TEARDOWN);
                currentTestcase.append(System.getProperty("line.separator")).append("}");
                currentTestcase.append(System.getProperty("line.separator")).append(System.getProperty("line.separator"));
                generatedTestcases.append(currentTestcase);
            });

            // my_logger.info(generatedTestcases.toString());
            return generatedTestcases.toString();
    }


    private String processVarValuePlaceholders(String value){
        return value.replaceAll("NULL", "");
    }

    private String processSyntaxPlaceholders(String value) {
            return value.replaceAll("\\*", ".").replaceAll("#LP#", "(").replaceAll("#RP#", ")");
    }

    private String processExpectedValuePlaceHolders(String value){
        if (value.isEmpty()){
            return value;
        }
        String pattern = "#LP#([a-zA-Z0-9_]+)#RP#";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(value);

        my_logger.debug(value);

        if (m.find()){
            String placeholder = m.group(1);
            try {
                String replacingString = Arrays.stream(StatusBarControlPatterns.values()).filter(item -> item.name().contentEquals(placeholder)).findFirst().get().getLabelname();
                return value.replaceAll(placeholder, '"'+replacingString+'"');
            }
            catch(NoSuchElementException ex){
                my_logger.info(placeholder+" is not supported");
                return value;
            }
        }
        else{
            throw new RuntimeException("expected value doesn't contain: "+pattern+" pattern");
        }
    }

}
