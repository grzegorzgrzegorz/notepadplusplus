package com.passfailerror.testcases_generator;

public class HasElement {

    String name;
    String value;

    public HasElement(){}

    public void setName(String name){this.name = name;}
    public void setValue(String value){this.value = value;}

    public String getName(){return name;}
    public String getValue(){return value;}

}
