import com.passfailerror.notepadplusplus.DSL.DSLTestCase;
import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.NewDocumentPreferencesControlPatterns;
import org.junit.*;
import org.junit.rules.TestName;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import static com.passfailerror.notepadplusplus.enums.NewDocumentPreferencesControlPatterns.*;

public class NotepadTestCases extends DSLTestCase{

    static TestDriver testDriver;

    @Rule
    public TestName name = new TestName();

    @BeforeClass
    public static void commonSetUp(){
        START_APPLICATION(NotepadTestCases.class.getCanonicalName());
    }

    @AfterClass
    public static void commonTearDown(){
        CLOSE_APPLICATION(NotepadTestCases.class.getCanonicalName());
    }

    @Before
    public void setUp(){
        SETUP(name.getMethodName());
    }


    @Test
    public void test(){
        WHEN().tab().isOpened().
        THEN().statusBar().shouldContain("Windows (CR LF)");
    }

    @Test
    public void testNewDocumentPreferences_tc0(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_ANSI).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_C).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("ANSI").
                THEN().statusBar().shouldContain("C source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc1(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_NORMAL).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UTF-8").
                THEN().statusBar().shouldContain("Normal text file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc2(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8_WITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_PHP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UTF-8-BOM").
                THEN().statusBar().shouldContain("PHP Hypertext Preprocessor file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc3(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_BIGENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CPLUSPLUS).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UCS-2 BE BOM").
                THEN().statusBar().shouldContain("C++ source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc4(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_LITTLEENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CSHARP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UCS-2 LE BOM").
                THEN().statusBar().shouldContain("C# source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc5(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_ANSI).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_C).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("ANSI").
                THEN().statusBar().shouldContain("C source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc6(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_NORMAL).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UTF-8").
                THEN().statusBar().shouldContain("Normal text file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc7(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8_WITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_PHP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UTF-8-BOM").
                THEN().statusBar().shouldContain("PHP Hypertext Preprocessor file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc8(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_BIGENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CPLUSPLUS).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UCS-2 BE BOM").
                THEN().statusBar().shouldContain("C++ source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc9(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_LITTLEENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CSHARP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UCS-2 LE BOM").
                THEN().statusBar().shouldContain("C# source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc10(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_ANSI).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_C).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("ANSI").
                THEN().statusBar().shouldContain("C source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc11(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_NORMAL).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("UTF-8").
                THEN().statusBar().shouldContain("Normal text file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc12(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8_WITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_PHP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("UTF-8-BOM").
                THEN().statusBar().shouldContain("PHP Hypertext Preprocessor file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc13(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_BIGENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CPLUSPLUS).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("UCS-2 BE BOM").
                THEN().statusBar().shouldContain("C++ source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc14(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_LITTLEENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CSHARP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("UCS-2 LE BOM").
                THEN().statusBar().shouldContain("C# source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc15(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_ANSI).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_NORMAL).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("ANSI").
                THEN().statusBar().shouldContain("Normal text file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc16(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_ANSI).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_PHP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("ANSI").
                THEN().statusBar().shouldContain("PHP Hypertext Preprocessor file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc17(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_ANSI).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CPLUSPLUS).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("ANSI").
                THEN().statusBar().shouldContain("C++ source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc18(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_ANSI).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CSHARP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("ANSI").
                THEN().statusBar().shouldContain("C# source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc19(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_C).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UTF-8").
                THEN().statusBar().shouldContain("C source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc20(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_PHP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UTF-8").
                THEN().statusBar().shouldContain("PHP Hypertext Preprocessor file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc21(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CPLUSPLUS).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UTF-8").
                THEN().statusBar().shouldContain("C++ source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc22(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CSHARP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UTF-8").
                THEN().statusBar().shouldContain("C# source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc23(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8_WITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_C).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UTF-8-BOM").
                THEN().statusBar().shouldContain("C source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc24(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8_WITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_NORMAL).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UTF-8-BOM").
                THEN().statusBar().shouldContain("Normal text file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc25(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8_WITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CPLUSPLUS).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("UTF-8-BOM").
                THEN().statusBar().shouldContain("C++ source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc26(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UTF8_WITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CSHARP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("UTF-8-BOM").
                THEN().statusBar().shouldContain("C# source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc27(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_BIGENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_C).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UCS-2 BE BOM").
                THEN().statusBar().shouldContain("C source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc28(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_BIGENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_NORMAL).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UCS-2 BE BOM").
                THEN().statusBar().shouldContain("Normal text file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc29(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_BIGENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_PHP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("UCS-2 BE BOM").
                THEN().statusBar().shouldContain("PHP Hypertext Preprocessor file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc30(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_BIGENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CSHARP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UCS-2 BE BOM").
                THEN().statusBar().shouldContain("C# source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc31(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_LITTLEENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_C).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UCS-2 LE BOM").
                THEN().statusBar().shouldContain("C source file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc32(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_UNIX).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_LITTLEENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_NORMAL).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Unix (LF)").
                THEN().statusBar().shouldContain("UCS-2 LE BOM").
                THEN().statusBar().shouldContain("Normal text file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc33(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_MAC).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_LITTLEENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_PHP).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Macintosh (CR)").
                THEN().statusBar().shouldContain("UCS-2 LE BOM").
                THEN().statusBar().shouldContain("PHP Hypertext Preprocessor file");
        TEARDOWN().tab().isClosed();
    }

    @Test
    public void testNewDocumentPreferences_tc34(){
        GIVEN().newDocumentFormat().isSetTo(FORMAT_WINDOWS).
                GIVEN().newDocumentEncoding().isSetTo(ENCODING_UCS2_LITTLEENDIANWITHBOM).
                GIVEN().newDocumentDefaultLanguage().isSetTo(DEFAULT_LANGUAGE_CPLUSPLUS).
                WHEN().newDocumentPreferences().isClosed().
                WHEN().tab().isOpened().
                THEN().statusBar().shouldContain("Windows (CR LF)").
                THEN().statusBar().shouldContain("UCS-2 LE BOM").
                THEN().statusBar().shouldContain("C++ source file");
        TEARDOWN().tab().isClosed();
    }


}