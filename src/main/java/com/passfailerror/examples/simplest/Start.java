package com.passfailerror.examples.simplest;

import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Start {


        public static void main(String[] args) {
            new Start();
        }


        public Start(){

            Logger my_logger = LogManager.getLogger(this.getClass().getName());
            my_logger.info("start");


            new App("C:\\Program Files (x86)\\Notepad++\\notepad++.exe").open();
            String title = ("Notepad++");
            new App(title).focus();
            Region windowArea = App.focusedWindow();
            windowArea.highlight(3f);

                try {
                    windowArea.hover(windowArea.getCenter());
                    Thread.sleep(1000);
                    windowArea.type("Sikuli is working");
                } catch (FindFailed findFailed) {
                    findFailed.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


        }



}
