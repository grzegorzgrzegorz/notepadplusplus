package com.passfailerror.notepadplusplus.state_transition;

import com.passfailerror.notepadplusplus.DSL.DSLTestCase;
import com.passfailerror.notepadplusplus.DSL.GivenOrWhen.GivenOrWhenStart;
import com.passfailerror.notepadplusplus.enums.Tabs;


public class StateTextLTR extends StateTabIsSelected {

    public StateTextLTR(StateDriver stateDriver, Tabs tab){
        super(stateDriver, tab);
    }

    public StateTextLTR assertTabIsWritable(){
        super.assertTabIsWritable();
        return new StateTextLTR(stateDriver, tab);
    }

    public StateTextLTR createNewTab(){
        super.createNewTab();
        return new StateTextLTR(stateDriver, tab);
    }

    public StateTextLTR selectTab(Tabs tab){
        super.selectTab(tab);
        return new StateTextLTR(stateDriver, tab);
    }

    public StateTextLTR deleteTab(Tabs tab){
        super.deleteTab(tab);
        return new StateTextLTR(stateDriver, tab);
    }


    public StateTextRTL changeTextDirection(){
        dslLogger.info("Transition: change text direction");
        new GivenOrWhenStart(DSLTestCase.testDriver).textDirectionIsChanged("RTL");
        stateDriver.setCurrentTextDirection("RTL");
        return new StateTextRTL(stateDriver, tab);
    }
}
