package com.passfailerror.notepadplusplus.state_transition;

import com.passfailerror.notepadplusplus.DSL.DSLTestCase;
import com.passfailerror.notepadplusplus.DSL.GivenOrWhen.GivenOrWhenStart;
import com.passfailerror.notepadplusplus.enums.Tabs;


public class StateTabIsSelected extends StateTransitionTestCase{


    Tabs tab;

    public StateTabIsSelected(StateDriver stateDriver, Tabs tab){
        super(stateDriver);
        this.tab = tab;
    }

    public StateTabIsSelected assertTabIsWritable(){
        new NotepadAssertion().tabIsWritable();
        return new StateTabIsSelected(stateDriver, tab);
    }

    public StateTabIsSelected createNewTab(){
        dslLogger.info("Transition: createNewTab");
        new GivenOrWhenStart(DSLTestCase.testDriver).tab().isOpened();
        Tabs nextTab = stateDriver.getNextTab();
        stateDriver.addAvailableTab(nextTab);
        stateDriver.setCurrentlySelectedTab(nextTab);
        return new StateTabIsSelected(stateDriver, nextTab);
    }

    public StateTabIsSelected selectTab(Tabs tab){
        dslLogger.info("Transition: selectTab");
        new GivenOrWhenStart(DSLTestCase.testDriver).tab(tab).isSelected();
        stateDriver.setCurrentlySelectedTab(tab);
        return new StateTabIsSelected(stateDriver, tab);
    }

    public StateTabIsSelected deleteTab(Tabs tab){
        dslLogger.info("Transition: deleteTab");
        new GivenOrWhenStart(DSLTestCase.testDriver).tab(tab).isDeleted();
        stateDriver.deleteAvailableTab(tab);
        return new StateTabIsSelected(stateDriver, tab);
    }

}
