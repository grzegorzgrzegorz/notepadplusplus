package com.passfailerror.notepadplusplus.state_transition;

import com.passfailerror.notepadplusplus.DSL.DSLTestCase;
import com.passfailerror.notepadplusplus.enums.Tabs;

public class StateTransitionTestCase extends DSLTestCase {


    protected StateDriver stateDriver;

    public StateTransitionTestCase(StateDriver stateDriver){this.stateDriver = stateDriver;}


    public StateTransitionTestCase(){}

    public StateTextLTR start() {
        dslLogger.info("START");
        StateDriver s = new StateDriver();
        return new StateTextLTR(s, Tabs.new1);
    }





}
