package com.passfailerror.notepadplusplus.state_transition;

import com.passfailerror.notepadplusplus.DSL.DSLTestCase;
import com.passfailerror.notepadplusplus.DSL.GivenOrWhen.GivenOrWhenStart;

import static org.hamcrest.CoreMatchers.*;

public class NotepadAssertion {

    public String text = "Sikuli can automate every kind of GUI.";

    public void tabIsWritable() {
        try {
            new GivenOrWhenStart(DSLTestCase.testDriver).tab().isWritten(text).
                    WHEN().tab().contentIsCopied().
                    THEN().clipboardContent().shouldBe(equalTo(text));
        }
        finally{
            new GivenOrWhenStart(DSLTestCase.testDriver).tab().contentIsCleared();
        }
    }

}
