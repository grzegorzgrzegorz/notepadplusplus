package com.passfailerror.notepadplusplus.state_transition;

import com.passfailerror.notepadplusplus.DSL.DSLTestCase;
import com.passfailerror.notepadplusplus.DSL.GivenOrWhen.GivenOrWhenStart;
import com.passfailerror.notepadplusplus.enums.Tabs;


public class StateTextRTL extends StateTabIsSelected{

    public StateTextRTL(StateDriver stateDriver, Tabs tab){
        super(stateDriver, tab);
    }


    public StateTextRTL assertTabIsWritable(){
        super.assertTabIsWritable();
        return new StateTextRTL(stateDriver, tab);
    }

    public StateTextRTL createNewTab(){
        super.createNewTab();
        return new StateTextRTL(stateDriver, tab);
    }

    public StateTextRTL selectTab(Tabs tab){
        super.selectTab(tab);
        return new StateTextRTL(stateDriver, tab);
    }

    public StateTextRTL deleteTab(Tabs tab){
        super.deleteTab(tab);
        return new StateTextRTL(stateDriver, tab);
    }


    public StateTextLTR changeTextDirection(){
        dslLogger.info("Transition: change text direction");
        new GivenOrWhenStart(DSLTestCase.testDriver).textDirectionIsChanged("LTR");
        stateDriver.setCurrentTextDirection("LTR");
        return new StateTextLTR(stateDriver, tab);
    }

}
