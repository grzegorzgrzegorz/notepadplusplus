package com.passfailerror.notepadplusplus.state_transition;

import com.passfailerror.notepadplusplus.enums.Tabs;

import java.util.ArrayList;
import java.util.List;


public class StateDriver {

    List<Tabs> availableTabs = new ArrayList();

    Tabs currentlySelectedTab;
    String currentTextDirection;

    public StateDriver(){
        currentlySelectedTab = Tabs.new1;
        availableTabs.add(Tabs.new1);
        currentTextDirection = "LTR";
    }

    public Tabs getNextTab(){
        int nextTabNo = currentlySelectedTab.getTabNo()+1;
        return currentlySelectedTab.getTabByTabNo(nextTabNo);
    }

    public void setCurrentlySelectedTab(Tabs tab){
        currentlySelectedTab = tab;
    }


    public void setCurrentTextDirection(String mode){
        currentTextDirection = mode;
    }

    public void addAvailableTab(Tabs tab){
        availableTabs.add(tab);
    }

    public void deleteAvailableTab(Tabs tab){
        availableTabs.remove(tab);
    }


}
