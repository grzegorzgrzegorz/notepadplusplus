package com.passfailerror.notepadplusplus;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sikuli.basics.Settings;

public class Sikuli_Logger {

	public static Logger logger = null;

	public static void getLogger() {
		logger = LogManager.getLogger(Sikuli_Logger.class.getName());
		if (get_logging_level() <= 400) {
			Settings.ActionLogs = false;
			Settings.InfoLogs = false;
			Settings.DebugLogs = false;
		} else if (get_logging_level() > 400) {
			Settings.ActionLogs = true;
			Settings.InfoLogs = true;
			Settings.DebugLogs = true;
		}

	}

	private static int get_logging_level() {
		int current_level = logger.getLevel().intLevel();
		return current_level;
	}

}
