package com.passfailerror.notepadplusplus.utils;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.Typing;
import org.sikuli.script.Region;


public class Keyboard {

    Region typingRegion;
    TestDriver testDriver;

    public Keyboard(TestDriver testDriver, Region typingRegion){
        this.testDriver = testDriver;
        this.typingRegion = typingRegion;
    }


    public void typeKeys(Object input, int repetitions, int pause) throws InterruptedException {
        try{
        if (input instanceof Typing){
            Typing label = (Typing) input;
            typeEnumeratedKeys(label, repetitions, pause);
        }
        else if (input instanceof String){
            String label = (String) input;
            typeKeysLiterally(label, repetitions, pause);
        }
        else
        {
            throw new RuntimeException("Unknown object type for typing.");
        }
        }
        catch(InterruptedException interrupted){
            throw new RuntimeException(interrupted);
        }
    }


    public void typeKeys(Object input) {
        try {
            if (input instanceof Typing) {
                Typing label = (Typing) input;
                typeEnumeratedKeys(label);
            } else if (input instanceof String) {
                String label = (String) input;
                typeKeysLiterally(label);
            } else {
                throw new RuntimeException("Unknown object type for typing.");
            }
        }
        catch(InterruptedException interrupted){
            throw new RuntimeException(interrupted);
        }
    }

    private void typeEnumeratedKeys(Typing label) throws InterruptedException {
        typeEnumeratedKeys(label, 1, 0);
    }

    private void typeEnumeratedKeys(Typing label, int repetitions, int pause)
            throws InterruptedException {
        for (int a = 1; a <= repetitions; a++) {
            switch (label.getKeys().size()){
                case 1:
                    typingRegion.type(label.getKeys().get(0));
                    break;

                case 2:
                    typingRegion.type(label.getKeys().get(0), Integer.valueOf(label.getKeys().get(1)));
                    break;

                case 3:
                    typingRegion.type(label.getKeys().get(0), Integer.valueOf(label.getKeys().get(1)) + Integer.valueOf(label.getKeys().get(2)));
                    break;

                default:
                    throw new RuntimeException("Wrong number of keys sent from enum");

            }
            if (pause > 0) {
                testDriver.user_waits(pause);
            }
        }

    }

    private void typeKeysLiterally(String label) throws InterruptedException {
        typeKeysLiterally(label, 1, 0);
    }

    private void typeKeysLiterally(String label, int repetitions, int pause)
            throws InterruptedException {
        for (int a = 1; a <= repetitions; a++) {
            typingRegion.type(label);
            if (pause > 0) {
                testDriver.user_waits(pause);
            }
        }

    }


}
