package com.passfailerror.notepadplusplus.runner;

import com.passfailerror.notepadplusplus.TestDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Start{


	public static void main(String[] args) throws Exception {
		new Start();
    }

    public Start() throws Exception{


        Logger my_logger = LogManager.getLogger(this.getClass().getName());
        my_logger.info("start");

        TestDriver testDriver = new TestDriver("C:\\Program Files (x86)\\Notepad++\\notepad++.exe", "Notepad++");
        Thread.sleep(2000);
        testDriver.mainWindowControl().menuControl().fileDropdownControl().newTab();
        testDriver.editingAreaControl().typeKeys("Sikuli can automate every kind of GUI.");
        testDriver.shortcutControl().closeTab();
        testDriver.shortcutControl().newTab();

        testDriver.editingAreaControl().typeKeys("Sikuli automates GUI using Java.");
        testDriver.shortcutControl().closeTab();

        testDriver.mainWindowControl().menuControl().fileDropdownControl().exit();


    }

}

