package com.passfailerror.notepadplusplus.DSL.Then;

import com.passfailerror.notepadplusplus.DSL.DSL;
import com.passfailerror.notepadplusplus.TestDriver;

public class Then extends DSL {


    public Then(TestDriver testDriver){
        super (testDriver);
    }


    public ThenStart THEN(){
        dslLogger.info("THEN");
        return new ThenStart(testDriver);
    }
}
