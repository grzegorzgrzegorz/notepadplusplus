package com.passfailerror.notepadplusplus.DSL.Then;


import com.passfailerror.notepadplusplus.HL_Logger;
import com.passfailerror.notepadplusplus.TestDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


public class ThenStatusBarContext extends Then {


    public ThenStatusBarContext(TestDriver testDriver){
        super(testDriver);
    }



    public ThenStatusBarContext shouldContain(String text){
        dslLogger.info("should contain \'"+text+"\' text");
        testDriver.createScreenshot(testDriver.getWindowRegion());
        boolean result;
        result = testDriver.mainWindowControl().statusBarControl().containsText(text);
        assertThat(result, equalTo(true));
        return new ThenStatusBarContext(testDriver);
    }


}
