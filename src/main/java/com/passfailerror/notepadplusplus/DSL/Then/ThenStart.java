package com.passfailerror.notepadplusplus.DSL.Then;

import com.passfailerror.notepadplusplus.DSL.DSL;
import com.passfailerror.notepadplusplus.TestDriver;

public class ThenStart extends DSL {

    public ThenStart(TestDriver testDriver){
        super(testDriver);
    }

    public ThenClipboardContext clipboardContent(){
        dslLogger.info("clipboard content");
        return new ThenClipboardContext(testDriver);
    }

    public ThenStatusBarContext statusBar(){
        dslLogger.info("status bar");
        return new ThenStatusBarContext(testDriver);
    }

}
