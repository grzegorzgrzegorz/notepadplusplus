package com.passfailerror.notepadplusplus.DSL.Then;


import com.passfailerror.notepadplusplus.TestDriver;
import org.hamcrest.Matcher;
import org.sikuli.script.Screen;
import org.sikuli.script.ScreenImage;

import static org.hamcrest.MatcherAssert.assertThat;


public class ThenClipboardContext extends Then {


    public ThenClipboardContext(TestDriver testDriver){
        super(testDriver);
    }



    public ThenClipboardContext shouldBe(Matcher matcher){
        dslLogger.info("should be "+matcher);
        String actual = testDriver.getClipboardContent();
        dslLogger.info("assertion, actual: [\"" + actual + "\"] expected: [" + matcher + "]");
        testDriver.createScreenshot(testDriver.getWindowRegion());

        assertThat(actual, matcher);
        return new ThenClipboardContext(testDriver);
    }



}
