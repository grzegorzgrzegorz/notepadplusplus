package com.passfailerror.notepadplusplus.DSL;

import com.passfailerror.notepadplusplus.DSL.GivenOrWhen.GivenOrWhenStart;
import com.passfailerror.notepadplusplus.TestDriver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class DSL {

    public static final Logger dslLogger = LogManager.getLogger(DSLTestCase.class.getName());



    public TestDriver testDriver;

    public DSL(TestDriver testDriver){
        this.testDriver = testDriver;


    }

    public DSL(){

    }

    public GivenOrWhenStart AND(){
        dslLogger.info("AND");
        return new GivenOrWhenStart(testDriver);
    }

}
