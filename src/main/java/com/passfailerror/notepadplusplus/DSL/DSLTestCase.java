package com.passfailerror.notepadplusplus.DSL;


import com.passfailerror.notepadplusplus.DSL.GivenOrWhen.GivenOrWhenStart;
import com.passfailerror.notepadplusplus.TestDriver;



public class DSLTestCase extends DSL{

    public static TestDriver testDriver;


    public GivenOrWhenStart GIVEN(){
         try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        dslLogger.info("GIVEN");
        return new GivenOrWhenStart(testDriver);
    }

    public GivenOrWhenStart WHEN(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        dslLogger.info("WHEN");
        return new GivenOrWhenStart(testDriver);
    }

    public static SetupOrTeardown START_APPLICATION(String name){
        dslLogger.info("START APPLICATION");
        dslLogger.info("Test case set: "+name);
        testDriver = new TestDriver("C:\\Program Files (x86)\\Notepad++\\notepad++.exe", "Notepad++", name);
        return new SetupOrTeardown(testDriver);}

    public static void CLOSE_APPLICATION(String testName){
        dslLogger.info("CLOSE APPLICATION");
        testDriver.setCurrentTestName(testName);
        new SetupOrTeardown(testDriver).window().isClosed();}

    public SetupOrTeardown TEARDOWN(){
        dslLogger.info("TEARDOWN");
        return new SetupOrTeardown(testDriver);
    }

    public SetupOrTeardown SETUP(String testName){
        dslLogger.info("SETUP");
        dslLogger.info("Test case: "+testName);
        testDriver.setCurrentTestName(testName);
        return new SetupOrTeardown(testDriver);
    }


}
