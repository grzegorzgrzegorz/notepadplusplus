package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;

public class GivenOrWhenNewDocumentPreferenceContext extends GivenOrWhen {



    public GivenOrWhenNewDocumentPreferenceContext(TestDriver testDriver){
        super(testDriver);
    }


    public GivenOrWhenNewDocumentPreferenceContext isOpened(){
        dslLogger.info("is opened");
        testDriver.mainWindowControl().menuControl().settingsDropdownControl().preferencesControl().newDocumentPreferenceControl();
        return new GivenOrWhenNewDocumentPreferenceContext(testDriver);
    }

    public GivenOrWhenNewDocumentPreferenceContext isClosed(){
        dslLogger.info("is closed");
        testDriver.mainWindowControl().menuControl().settingsDropdownControl().preferencesControl().newDocumentPreferenceControl().close();

        return new GivenOrWhenNewDocumentPreferenceContext(testDriver);
    }

}
