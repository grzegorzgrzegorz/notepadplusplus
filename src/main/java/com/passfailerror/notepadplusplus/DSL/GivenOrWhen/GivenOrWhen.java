package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.DSL.DSL;
import com.passfailerror.notepadplusplus.DSL.Then.ThenStart;
import com.passfailerror.notepadplusplus.TestDriver;

public class GivenOrWhen extends DSL {


    public GivenOrWhen(TestDriver testDriver){
        super (testDriver);
    }

    public ThenStart THEN(){
        dslLogger.info("THEN");
        return new ThenStart(testDriver);
    }

    public GivenOrWhenStart WHEN(){
        dslLogger.info("WHEN");
        return new GivenOrWhenStart(testDriver);
    }

    public GivenOrWhenStart GIVEN(){
        dslLogger.info("GIVEN");
        return new GivenOrWhenStart(testDriver);
    }


}
