package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.NewDocumentPreferencesControlPatterns;

public class GivenOrWhenNewDocumentPreferenceEncodingContext extends GivenOrWhen {



    public GivenOrWhenNewDocumentPreferenceEncodingContext(TestDriver testDriver){
        super(testDriver);
    }


    public GivenOrWhenNewDocumentPreferenceEncodingContext isSetTo(NewDocumentPreferencesControlPatterns mapper){
        dslLogger.info("is set to: "+mapper.name());
        testDriver.mainWindowControl().menuControl().settingsDropdownControl().preferencesControl().newDocumentPreferenceControl().setEncoding(mapper);
        return new GivenOrWhenNewDocumentPreferenceEncodingContext(testDriver);
    }



}
