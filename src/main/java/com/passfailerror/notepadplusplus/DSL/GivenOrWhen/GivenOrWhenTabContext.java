package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.Tabs;
import com.passfailerror.notepadplusplus.enums.Typing;

public class GivenOrWhenTabContext extends GivenOrWhen {


    Tabs tab;

    public GivenOrWhenTabContext(TestDriver testDriver, Tabs tab){
        super(testDriver);
        this.tab = tab;
    }

    public GivenOrWhenTabContext isOpened(){
        dslLogger.info("is opened");
        testDriver.shortcutControl().newTab();
        return new GivenOrWhenTabContext(testDriver, tab);
    }

    public GivenOrWhenTabContext isClosed() {
        dslLogger.info("is closed");
        testDriver.shortcutControl().closeTab();
        return new GivenOrWhenTabContext(testDriver, tab);
    }

    public GivenOrWhenTabContext contentIsCopied(){
        dslLogger.info("content is copied");
        testDriver.shortcutControl().copyAllToClipboard();
        return new GivenOrWhenTabContext(testDriver, tab);
    }

    public GivenOrWhenTabContext contentIsCleared(){
        dslLogger.info("content is cleared");
        testDriver.shortcutControl().selectAll();
        testDriver.editingAreaControl().typeKeys(Typing.DELETE);
        return new GivenOrWhenTabContext(testDriver, tab);
    }

    public GivenOrWhenTabContext isWritten(String text){
        dslLogger.info("is written: \""+text+"\"");
        testDriver.editingAreaControl().click();
        testDriver.editingAreaControl().typeKeys(text);
        return new GivenOrWhenTabContext(testDriver, tab);
    }

    public GivenOrWhenTabContext isSelected(){
        dslLogger.info("is selected");
        testDriver.editingAreaControl().selectTab(tab);
        return new GivenOrWhenTabContext(testDriver, tab);
    }

    public GivenOrWhenTabContext isDeleted(){
        dslLogger.info("is deleted");
        testDriver.editingAreaControl().deleteTab(tab);
        return new GivenOrWhenTabContext(testDriver, tab);
    }

}
