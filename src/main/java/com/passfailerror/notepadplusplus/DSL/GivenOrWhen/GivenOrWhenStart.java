package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.DSL.DSL;
import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.Tabs;

public class GivenOrWhenStart extends DSL {


    public GivenOrWhenStart(TestDriver testDriver){
        super(testDriver);
    }



    public GivenOrWhenNewDocumentPreferenceFormatContext newDocumentFormat(){
        dslLogger.info("new document format");
        return new GivenOrWhenNewDocumentPreferenceFormatContext(testDriver);
    }

    public GivenOrWhenNewDocumentPreferenceLanguageContext newDocumentDefaultLanguage(){
        dslLogger.info("new document language");
        return new GivenOrWhenNewDocumentPreferenceLanguageContext(testDriver);
    }

    public GivenOrWhenNewDocumentPreferenceEncodingContext newDocumentEncoding(){
        dslLogger.info("new document encoding");
        return new GivenOrWhenNewDocumentPreferenceEncodingContext(testDriver);
    }

    public GivenOrWhenNewDocumentPreferenceContext newDocumentPreferences(){
        dslLogger.info("new document preferences");
        return new GivenOrWhenNewDocumentPreferenceContext(testDriver);
    }

    public GivenOrWhenTabContext tab(){
        dslLogger.info("tab");
        return new GivenOrWhenTabContext(testDriver, Tabs.new1);
    }

    public GivenOrWhenTabContext tab(Tabs tab){
        dslLogger.info("tab "+tab.getFilename());
        return new GivenOrWhenTabContext(testDriver, tab);
    }

    public GivenOrWhenKeyboardContext text(){
        dslLogger.info("text");
        return new GivenOrWhenKeyboardContext(testDriver);
    }

    public GivenOrWhenWindowContext window(){
        dslLogger.info("window");
        return new GivenOrWhenWindowContext(testDriver);
    }

    public GivenOrWhenStart textDirectionIsChanged(String mode){
        dslLogger.info("text direction is changed to "+mode);
        if (mode.contentEquals("LTR")) {
            testDriver.shortcutControl().changeTextToLTR();
        }
        else if (mode.contentEquals("RTL")) {
            testDriver.shortcutControl().changeTextToRTL();
        }
         else{
                throw new RuntimeException("no such mode is supported: "+mode);
        }
        return new GivenOrWhenStart(testDriver);
    }


}
