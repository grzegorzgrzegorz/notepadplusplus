package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.NewDocumentPreferencesControlPatterns;

public class GivenOrWhenNewDocumentPreferenceLanguageContext extends GivenOrWhen {



    public GivenOrWhenNewDocumentPreferenceLanguageContext(TestDriver testDriver){
        super(testDriver);
    }


    public GivenOrWhenNewDocumentPreferenceLanguageContext isSetTo(NewDocumentPreferencesControlPatterns mapper){
        dslLogger.info("is set to: "+mapper.name());
        testDriver.mainWindowControl().menuControl().settingsDropdownControl().preferencesControl().newDocumentPreferenceControl().setDefaultLanguage(mapper);
        return new GivenOrWhenNewDocumentPreferenceLanguageContext(testDriver);
    }



}
