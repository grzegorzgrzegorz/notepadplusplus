package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;

public class GivenOrWhenKeyboardContext extends GivenOrWhen {

    public GivenOrWhenKeyboardContext(TestDriver testDriver){
        super (testDriver);
    }

    public GivenOrWhenKeyboardContext isTyped(String text){
        dslLogger.info("is typed");
        testDriver.editingAreaControl().typeKeys(text);
        return new GivenOrWhenKeyboardContext(testDriver);
    }


}
