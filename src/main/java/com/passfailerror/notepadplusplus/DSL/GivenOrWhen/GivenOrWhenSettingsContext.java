package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.Tabs;
import com.passfailerror.notepadplusplus.enums.Typing;

public class GivenOrWhenSettingsContext extends GivenOrWhen {


    public GivenOrWhenSettingsContext(TestDriver testDriver){
        super(testDriver);
    }


    public GivenOrWhenSettingsContext isOpened(){
        dslLogger.info("is opened");
        testDriver.menuControl().settingsDropdownControl();
        return new GivenOrWhenSettingsContext(testDriver);
    }


    public GivenOrWhenPreferencesContext preferences(){
        dslLogger.info("preferences");
        testDriver.menuControl().settingsDropdownControl().preferencesControl();
        return new GivenOrWhenPreferencesContext(testDriver);
    }


}
