package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;

public class GivenOrWhenWindowContext extends GivenOrWhen {

    public GivenOrWhenWindowContext(TestDriver testDriver){
        super (testDriver);
    }

    public GivenOrWhenWindowContext isClosed(){
        dslLogger.info("is closed");
        testDriver.shortcutControl().closeWindow();
        return new GivenOrWhenWindowContext(testDriver);
    }


}
