package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.Tabs;

public class GivenOrWhenPreferencesContext extends GivenOrWhen {


    public GivenOrWhenPreferencesContext(TestDriver testDriver){
        super(testDriver);
    }

    public GivenOrWhenPreferencesContext isOpened(){
        dslLogger.info("is opened");
        testDriver.menuControl().settingsDropdownControl().preferencesControl();
        return new GivenOrWhenPreferencesContext(testDriver);
    }


    public GivenOrWhenNewDocumentPreferenceContext new_document_preference(){
        dslLogger.info("new document preference");
        testDriver.menuControl().settingsDropdownControl().preferencesControl().newDocumentPreferenceControl();
        return new GivenOrWhenNewDocumentPreferenceContext(testDriver);
    }

}
