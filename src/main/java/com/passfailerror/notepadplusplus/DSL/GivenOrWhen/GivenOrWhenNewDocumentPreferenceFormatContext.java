package com.passfailerror.notepadplusplus.DSL.GivenOrWhen;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.NewDocumentPreferencesControlPatterns;

public class GivenOrWhenNewDocumentPreferenceFormatContext extends GivenOrWhen {



    public GivenOrWhenNewDocumentPreferenceFormatContext(TestDriver testDriver){
        super(testDriver);
    }


    public GivenOrWhenNewDocumentPreferenceFormatContext isSetTo(NewDocumentPreferencesControlPatterns mapper){
        dslLogger.info("is set to: "+mapper.name());
        testDriver.mainWindowControl().menuControl().settingsDropdownControl().preferencesControl().newDocumentPreferenceControl().setFormat(mapper);
        return new GivenOrWhenNewDocumentPreferenceFormatContext(testDriver);
    }



}
