package com.passfailerror.notepadplusplus.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import com.passfailerror.notepadplusplus.HL_Logger;
import com.passfailerror.notepadplusplus.Sikuli_Logger;
import com.passfailerror.notepadplusplus.TestDriver;
import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;

public class MainWindowControl{
	App starting_app;
	App running_app;

	// Logger
	Logger my_logger;

    Region mainWindowRegion;
	EditingAreaControl editingAreaControl;
	MenuControl menuControl;
	StatusBarControl statusBarControl;
	TestDriver testDriver;

	public MainWindowControl(TestDriver testDriver, String appPath, String appName) {
        this.testDriver = testDriver;
        my_logger = LogManager.getLogger(this.getClass().getName());
        Sikuli_Logger.getLogger();
        HL_Logger.getLogger();
        my_logger.debug("Notepadplusplus_window constructor");

        new App(appPath).open();
        String title = appName;
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new App(title).focus();
        this.mainWindowRegion = App.focusedWindow();
        try {
            mainWindowRegion.click(mainWindowRegion.getCenter());
        } catch (FindFailed findFailed) {
            throw new RuntimeException(findFailed);
        }
        HL_Logger.debug(mainWindowRegion);
	}


    public MenuControl menuControl(){
        if (menuControl != null){
            return menuControl;
        }
        else {
            this.menuControl = new MenuControl(testDriver);
            return menuControl;
        }
    }

    public StatusBarControl statusBarControl(){
        if (statusBarControl != null){
            return statusBarControl;
        }
        else {
            this.statusBarControl = new StatusBarControl(testDriver, mainWindowRegion);
            return statusBarControl;
        }
    }

    public EditingAreaControl editingAreaControl(){
        if (editingAreaControl != null){
            return editingAreaControl;
        }
                else {
            this.editingAreaControl = new EditingAreaControl(testDriver);
            return editingAreaControl;
        }
    }


    public Region getRegion(){
        return mainWindowRegion;
    }








}
