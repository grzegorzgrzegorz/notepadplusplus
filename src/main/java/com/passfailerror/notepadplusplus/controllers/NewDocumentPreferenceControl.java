package com.passfailerror.notepadplusplus.controllers;

import com.passfailerror.notepadplusplus.HL_Logger;
import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.NewDocumentPreferencesControlPatterns;
import com.passfailerror.notepadplusplus.enums.PreferencesControlPatterns;
import com.passfailerror.notepadplusplus.enums.SettingsDropdownControlPatterns;
import com.passfailerror.notepadplusplus.enums.Typing;
import com.passfailerror.notepadplusplus.utils.Keyboard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import static com.passfailerror.notepadplusplus.DSL.DSLTestCase.testDriver;



public class NewDocumentPreferenceControl extends RegionControl {

    private TestDriver testDriver;
    Region newDocumentRegion;

    public NewDocumentPreferenceControl(TestDriver testDriver, Region preferencesRegion) { // this should click preferences
        this.testDriver = testDriver;
        this.newDocumentRegion = new Region(preferencesRegion.getX()+preferencesRegion.getW(), preferencesRegion.getY(), preferencesRegion.getW()*5, preferencesRegion.getH()*2);
        HL_Logger.debug(newDocumentRegion);
    }

    @Override
    protected Pattern getPattern(Enum mapper){
        NewDocumentPreferencesControlPatterns specMapper = (NewDocumentPreferencesControlPatterns) mapper;
        String fileName = specMapper.getFilename();
        return new Pattern(loader.getResource("screenshots/" + this.getClass().getSimpleName() + "/" + fileName + ".png").getFile()).similar(0.9f);
    }

    public void doKeyboardAction(Enum mapper){
        NewDocumentPreferencesControlPatterns specMapper = (NewDocumentPreferencesControlPatterns) mapper;
        String key = specMapper.getKey();
        Integer repetitions = specMapper.getRepetitions();
        try {
            new Keyboard(testDriver, newDocumentRegion).typeKeys(key, repetitions, 0);
            new Keyboard(testDriver, newDocumentRegion).typeKeys(Typing.ENTER);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void doAction(Pattern result){
        try {
            newDocumentRegion.click(result);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }

    public Region getRegion(){
        return newDocumentRegion;
    }



    public void setFormat(NewDocumentPreferencesControlPatterns mapper){
        doAction(getPattern(mapper));
    }

    public void setDefaultLanguage(NewDocumentPreferencesControlPatterns mapper){
        doAction(getPattern(NewDocumentPreferencesControlPatterns.DEFAULT_LANGUAGE_LABEL).targetOffset(150,0));
        new Keyboard(testDriver, newDocumentRegion).typeKeys(Typing.PAGE_UP);
        doKeyboardAction(mapper);
    }

    public void setEncoding(NewDocumentPreferencesControlPatterns mapper){
        if (mapper.getFilename() != null){
            doAction(getPattern(mapper));
        }
        else{
            doAction(getPattern(NewDocumentPreferencesControlPatterns.ENCODING_UCS2_LITTLEENDIANWITHBOM).targetOffset(0,50));
            new Keyboard(testDriver, newDocumentRegion).typeKeys(Typing.PAGE_UP);
            doKeyboardAction(mapper);
        }

    }

    public void close(){
        doAction(getPattern(NewDocumentPreferencesControlPatterns.BUTTON_CLOSE));
        testDriver.menuControl().clearAllDropdownControls();
    }

}
