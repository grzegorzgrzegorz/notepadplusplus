package com.passfailerror.notepadplusplus.controllers;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.utils.Keyboard;

import static com.passfailerror.notepadplusplus.enums.Typing.*;


public class ShortcutControl {

    TestDriver testDriver;
    Keyboard keyboard;
    public ShortcutControl(TestDriver testDriver) {
    this.testDriver = testDriver;
        this.keyboard = new Keyboard(testDriver, testDriver.getWindowRegion());
    }

    public void newTab() {
        keyboard.typeKeys(CTRL_N);
    }

    public void closeTab(){
        keyboard.typeKeys(CTRL_W);
    }

    public void copyAllToClipboard(){
        testDriver.clearClipboard();
        keyboard.typeKeys(CTRL_A);
        keyboard.typeKeys(CTRL_C);
    }

    public void selectAll(){
        keyboard.typeKeys(CTRL_A);
    }

    public void closeWindow(){
        keyboard.typeKeys(ALT_F4);
    }

    public void changeTextToLTR(){
        keyboard.typeKeys(CTRL_ALT_L);
    }

    public void changeTextToRTL(){
        keyboard.typeKeys(CTRL_ALT_R);
    }

}
