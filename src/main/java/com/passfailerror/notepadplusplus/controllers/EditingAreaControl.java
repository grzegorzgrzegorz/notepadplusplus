package com.passfailerror.notepadplusplus.controllers;

import com.passfailerror.notepadplusplus.HL_Logger;
import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.Tabs;
import com.passfailerror.notepadplusplus.enums.Typing;
import com.passfailerror.notepadplusplus.utils.Keyboard;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class EditingAreaControl extends RegionControl{

private TestDriver testDriver;
private Region editingAreaRegion;
    private Region tabRegion;
private Keyboard keyboard;

    public EditingAreaControl(TestDriver testDriver) {

        Logger my_logger = LogManager.getLogger(this.getClass().getName());
        my_logger.debug("recognizing writing area");
        this.testDriver = testDriver;
        this.editingAreaRegion = Region.create(testDriver.menuControl().getRegion().getX(), testDriver.menuControl().getRegion().getY()
                        + testDriver.menuControl().getRegion().getH()*2, testDriver.getWindowRegion().getW(), testDriver.getWindowRegion().getH() - testDriver.menuControl().getRegion().getH()*2);
        this.keyboard = new Keyboard(testDriver, editingAreaRegion);
        HL_Logger.debug(this.editingAreaRegion);
        this.tabRegion = Region.create(editingAreaRegion.getTopLeft().getX(), editingAreaRegion.getTopLeft().getY(), editingAreaRegion.getW(), 35);
        HL_Logger.debug(this.tabRegion);
    }


    public Region getRegion(){
        return editingAreaRegion;
    }

    public void typeKeys(String string){
        keyboard.typeKeys(string);
    }

    public void typeKeys(Typing control){
        keyboard.typeKeys(control);
    }

    public void click(){
        editingAreaRegion.click();
    }

    public void doubleClick(){
        editingAreaRegion.doubleClick();
    }



    @Override
    protected Pattern getPattern(Enum mapper) {

        Tabs specMapper = (Tabs) mapper;
        String fileName = specMapper.getFilename();
        return new Pattern(loader.getResource("screenshots/" + this.getClass().getSimpleName() + "/" + fileName + ".png").getFile()).similar(0.9f);

    }

    protected Pattern getInactivePattern(Enum mapper) {

        Tabs specMapper = (Tabs) mapper;
        String inactiveFileName = specMapper.getInactiveFilename();
        return new Pattern(loader.getResource("screenshots/" + this.getClass().getSimpleName() + "/" + inactiveFileName + ".png").getFile()).similar(0.9f);

    }
    @Override
    protected void doAction(Pattern pattern) {

    }

    public void selectTab(Tabs tab) {
        try {
            tabRegion.find(getPattern(tab)).click();
        } catch (FindFailed ignored) {
            try {
                tabRegion.find(getInactivePattern(tab)).click();
            } catch (FindFailed findFailed) {
                throw new RuntimeException(findFailed);
            }
        }
    }

    public void deleteTab(Tabs tab){
        selectTab(tab);
        testDriver.shortcutControl().closeTab();
    }

}
