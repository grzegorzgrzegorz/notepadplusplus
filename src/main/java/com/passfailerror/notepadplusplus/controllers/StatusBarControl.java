package com.passfailerror.notepadplusplus.controllers;

import com.passfailerror.notepadplusplus.HL_Logger;
import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.MenuControlPatterns;
import com.passfailerror.notepadplusplus.enums.StatusBarControlPatterns;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import static com.passfailerror.notepadplusplus.enums.MenuControlPatterns.FILE;
import static com.passfailerror.notepadplusplus.enums.MenuControlPatterns.SETTINGS;

public class StatusBarControl extends RegionControl {

    private Region statusBarRegion;
    private TestDriver testDriver;


    public StatusBarControl(TestDriver testDriver, Region mainWindowRegion){
        Logger my_logger = LogManager.getLogger(this.getClass().getName());
        my_logger.debug("recognizing status bar region");
        this.testDriver = testDriver;
        this.statusBarRegion = new Region(mainWindowRegion.getX(),mainWindowRegion.getY()+mainWindowRegion.getH()-50,mainWindowRegion.getW(),50);
        HL_Logger.debug(statusBarRegion);
    }


    public Region getRegion(){
        return statusBarRegion;
    }

    @Override
    protected Pattern getPattern(Enum mapper){
        StatusBarControlPatterns specMapper = (StatusBarControlPatterns) mapper;
        String fileName = specMapper.getFilename();
        return new Pattern(loader.getResource("screenshots/" + this.getClass().getSimpleName() + "/" + fileName +".png").getFile());
    }

    @Override
    protected void doAction(Pattern pattern) {

    }


    public boolean containsText(String text){
        boolean result;
        try {
            Match textAsPattern = testDriver.mainWindowControl().statusBarControl().getRegion().find(textToPattern(text));
            HL_Logger.info(textAsPattern);
            result = true;
        } catch (FindFailed findFailed) {
            result = false;
        }
        return result;
    }



    public Pattern textToPattern(String text){
        StatusBarControlPatterns result=null;
        for(StatusBarControlPatterns  item: StatusBarControlPatterns.values()){
            if (item.getLabelname().contentEquals(text)){
                result = item;

            }
        }
        if (result == null){
            throw new RuntimeException("this text is not supported: "+text);
        }

        return getPattern(result);

    }

}
