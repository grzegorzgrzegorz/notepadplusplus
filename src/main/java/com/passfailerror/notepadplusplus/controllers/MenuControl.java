package com.passfailerror.notepadplusplus.controllers;

import com.passfailerror.notepadplusplus.controllers.FileDropdownControl;
import com.passfailerror.notepadplusplus.controllers.RegionControl;
import com.passfailerror.notepadplusplus.HL_Logger;
import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.MenuControlPatterns;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.passfailerror.notepadplusplus.enums.MenuControlPatterns.FILE;
import static com.passfailerror.notepadplusplus.enums.MenuControlPatterns.SETTINGS;

public class MenuControl extends RegionControl {

    private Region menuRegion;
    private FileDropdownControl fileDropdownControl;
    private SettingsDropdownControl settingsDropdownControl;
    private TestDriver testDriver;
    Region fileRegion;
    Region settingsRegion;

    public MenuControl(TestDriver testDriver){
        Logger my_logger = LogManager.getLogger(this.getClass().getName());
        my_logger.debug("recognizing menu region");
        this.testDriver = testDriver;
        try {
            this.fileRegion = testDriver.getWindowRegion().find(getPattern(FILE));
            this.settingsRegion = testDriver.getWindowRegion().find(getPattern(SETTINGS));
            this.menuRegion = Region.create(fileRegion.getX(),
                    fileRegion.getY() - 5, testDriver.getWindowRegion().getW(),
                    fileRegion.getH() + 10);
            HL_Logger.debug(menuRegion);
        } catch (FindFailed e) {
            my_logger.fatal("menu region not found");
            throw new RuntimeException(e);
        }

    }

    public void clearAllDropdownControls(){
        fileDropdownControl = null;
        settingsDropdownControl = null;
    }

    public FileDropdownControl fileDropdownControl() {
        if (fileDropdownControl == null) {
            this.fileDropdownControl = new FileDropdownControl(testDriver, fileRegion);
        }
        return fileDropdownControl;
    }

    public SettingsDropdownControl settingsDropdownControl() {
        if (settingsDropdownControl == null) {
            this.settingsDropdownControl = new SettingsDropdownControl(testDriver, settingsRegion);
        }
        return settingsDropdownControl;
    }


    public Region getRegion(){
        return menuRegion;
    }

    @Override
    protected Pattern getPattern(Enum mapper){
        MenuControlPatterns specMapper = (MenuControlPatterns) mapper;
        String fileName = specMapper.getFilename();
        return new Pattern(loader.getResource("screenshots/" + this.getClass().getSimpleName() + "/" + fileName +".png").getFile());
    }

    @Override
    protected void doAction(Pattern pattern) {

    }


}
