package com.passfailerror.notepadplusplus.controllers;

import com.passfailerror.notepadplusplus.HL_Logger;
import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.PreferencesControlPatterns;
import com.passfailerror.notepadplusplus.enums.SettingsDropdownControlPatterns;
import com.passfailerror.notepadplusplus.utils.Keyboard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import static com.passfailerror.notepadplusplus.DSL.DSLTestCase.testDriver;


public class PreferencesControl extends RegionControl {


    private TestDriver testDriver;
    private Region preferencesRegion; // preferences selection list region
    private PreferencesControl preferencesControl;
    private NewDocumentPreferenceControl newDocumentPreferenceControl;


    public PreferencesControl(TestDriver testDriver, Region preferencesLauncherRegion) { // this should click preferences
        Logger my_logger = LogManager.getLogger(this.getClass().getName());
        this.testDriver = testDriver;
        preferencesLauncherRegion.click();
        try { // recognize preferences selection list
            Region topLeft = testDriver.getWindowRegion().find(getPattern(PreferencesControlPatterns.GENERAL));
           preferencesRegion = new Region(topLeft.getX(), topLeft.getY(), 150, 300);
           HL_Logger.debug(preferencesRegion);

        } catch (FindFailed retry) {
            try{
                Region topLeft = testDriver.getWindowRegion().find(getPattern(PreferencesControlPatterns.GENERAL2));
                preferencesRegion = new Region(topLeft.getX(), topLeft.getY(), 150, 300);
            }
            catch (FindFailed findFailed) {
                throw new RuntimeException(findFailed);
            }
        }

    }

    @Override
    protected Pattern getPattern(Enum mapper){
        PreferencesControlPatterns specMapper = (PreferencesControlPatterns) mapper;
        String fileName = specMapper.getFilename();
        return new Pattern(loader.getResource("screenshots/" + this.getClass().getSimpleName() + "/" + fileName + ".png").getFile()).similar(0.9f);
    }


    public NewDocumentPreferenceControl newDocumentPreferenceControl() {
        if (newDocumentPreferenceControl == null){
            preferencesRegion.click();
            new Keyboard(testDriver, preferencesRegion).typeKeys("n");
            this.newDocumentPreferenceControl = new NewDocumentPreferenceControl(testDriver, preferencesRegion);
        }
        return this.newDocumentPreferenceControl;
}


    @Override
    protected void doAction(Pattern result){
        try {
            preferencesRegion.click(result);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }

    public Region getRegion(){
        return preferencesRegion;
    }
}
