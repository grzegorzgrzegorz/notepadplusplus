package com.passfailerror.notepadplusplus.controllers;

import com.passfailerror.notepadplusplus.HL_Logger;
import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.FileDropdownControlPatterns;
import com.passfailerror.notepadplusplus.enums.SettingsDropdownControlPatterns;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import static com.passfailerror.notepadplusplus.DSL.DSLTestCase.testDriver;


public class SettingsDropdownControl extends RegionControl {

    private TestDriver testDriver;
    private Region settingsDropdownRegion;
    private Region settingsRegion;
    private Region preferencesRegion;

    private PreferencesControl preferencesControl;


    public SettingsDropdownControl(TestDriver testDriver, Region settingsRegion) {
        Logger my_logger = LogManager.getLogger(this.getClass().getName());
        this.testDriver = testDriver;
        my_logger.debug("recognizing settings dropdown");
        this.settingsRegion = settingsRegion;
        settingsRegion.click();
        HL_Logger.debug(settingsRegion.grow(0,200,0,0));
        try {
            Region topLeft = settingsRegion.grow(0, 200, 0, 0).below().find(getPattern(SettingsDropdownControlPatterns.PREFERENCES));
            //HL_Logger.debug(topLeft);
            //HL_Logger.debug(topLeft.grow(30));
            this.preferencesRegion = topLeft;
            Region bottomLeft = settingsRegion.grow(0, 200, 0, 0).below().find(getPattern(SettingsDropdownControlPatterns.EDIT_POPUP_CONTEXT_MENU));
            bottomLeft = bottomLeft.grow(10);
            //HL_Logger.debug(bottomLeft);
            settingsDropdownRegion = new Region(topLeft.getX(), topLeft.getY(), 300, bottomLeft.getY() * bottomLeft.getH() - topLeft.getY());
            HL_Logger.debug(settingsDropdownRegion);

        } catch (FindFailed findFailed) {
            throw new RuntimeException(findFailed);
        }

    }

    public PreferencesControl preferencesControl() {
        if (preferencesControl == null) {
            this.preferencesControl = new PreferencesControl(testDriver, preferencesRegion);
        }
        return preferencesControl;
    }

    @Override
    protected Pattern getPattern(Enum mapper){
        SettingsDropdownControlPatterns specMapper = (SettingsDropdownControlPatterns) mapper;
        String fileName = specMapper.getFilename();
        return new Pattern(loader.getResource("screenshots/" + this.getClass().getSimpleName() + "/" + fileName + ".png").getFile()).similar(0.9f);
    }




    @Override
    protected void doAction(Pattern result){
        try {
            settingsDropdownRegion.click(result);
            settingsDropdownRegion.waitVanish(result);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }

    public Region getRegion(){
        return settingsDropdownRegion;
    }
}
