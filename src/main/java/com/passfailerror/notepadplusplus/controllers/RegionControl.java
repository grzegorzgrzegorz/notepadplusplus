package com.passfailerror.notepadplusplus.controllers;

import org.sikuli.script.Pattern;

public abstract class RegionControl {


    static public final ClassLoader loader = RegionControl.class.getClassLoader();

    protected abstract Pattern getPattern(Enum mapper);
    protected abstract void doAction(Pattern pattern);


}
