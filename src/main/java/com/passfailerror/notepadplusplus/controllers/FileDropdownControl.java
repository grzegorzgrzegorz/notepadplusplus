package com.passfailerror.notepadplusplus.controllers;

import com.passfailerror.notepadplusplus.TestDriver;
import com.passfailerror.notepadplusplus.enums.FileDropdownControlPatterns;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class FileDropdownControl extends RegionControl {

    private TestDriver testDriver;
    private Region fileDropdownRegion;
    private Region fileRegion;


    public FileDropdownControl(TestDriver testDriver, Region fileRegion) {
        Logger my_logger = LogManager.getLogger(this.getClass().getName());
        this.testDriver = testDriver;
        my_logger.debug("recognizing file dropdown");
        this.fileRegion = fileRegion;
        fileRegion.click();
        //HL_Logger.config(fileRegion.grow(0,100,0,0));
        try {
            Region topLeft = fileRegion.grow(0, 100, 0, 0).below().find(getPattern(FileDropdownControlPatterns.NEW_TAB));
            //HL_Logger.config(topLeft);
            Region topRight = topLeft.grow(30).right().find(getPattern(FileDropdownControlPatterns.RIGHT_BORDER));
            //HL_Logger.config(topRight);
            //HL_Logger.config(topLeft.grow(30));
            Region bottomLeft = topLeft.grow(30).below().find(getPattern(FileDropdownControlPatterns.EXIT));
            bottomLeft = bottomLeft.grow(10);
            //HL_Logger.config(bottomLeft);
            fileDropdownRegion = new Region(topLeft.getX(), topLeft.getY(), topRight.getX() - topLeft.getY(), bottomLeft.getY() * bottomLeft.getH() - topLeft.getY());
            //HL_Logger.config(fileDropdownRegion);

        } catch (FindFailed findFailed) {
            throw new RuntimeException(findFailed);
        }

    }




    @Override
    protected Pattern getPattern(Enum mapper){
        FileDropdownControlPatterns specMapper = (FileDropdownControlPatterns) mapper;
        String fileName = specMapper.getFilename();
        return new Pattern(loader.getResource("screenshots/" + this.getClass().getSimpleName() + "/" + fileName + ".png").getFile()).similar(0.9f);
    }



    public void newTab() {
        Pattern result = getPattern(FileDropdownControlPatterns.NEW_TAB);
        doAction(result);
}

    public void exit() {
        Pattern result = getPattern(FileDropdownControlPatterns.EXIT);
        doAction(result);
        }

    @Override
    protected void doAction(Pattern result){
        try {
            fileDropdownRegion.click(result);
            fileDropdownRegion.waitVanish(result);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
    }

    public Region getRegion(){
        return fileDropdownRegion;
    }
}
