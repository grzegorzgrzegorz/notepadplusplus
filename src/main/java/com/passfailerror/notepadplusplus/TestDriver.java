package com.passfailerror.notepadplusplus;

import com.passfailerror.notepadplusplus.DSL.DSL;
import com.passfailerror.notepadplusplus.controllers.EditingAreaControl;
import com.passfailerror.notepadplusplus.controllers.MainWindowControl;
import com.passfailerror.notepadplusplus.controllers.MenuControl;
import com.passfailerror.notepadplusplus.controllers.ShortcutControl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sikuli.script.App;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Region;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;



import javax.imageio.ImageIO;

public class TestDriver {

    private MainWindowControl mainWindowControl;
    private ShortcutControl shortcutControl;
    private String testName;

    String appPath;
    String appName;

    public static String screenshotDir = "c:/temp/";


    public static final Logger frameworkLogger = LogManager.getLogger(TestDriver.class.getName());

    public TestDriver(String appPath, String appName, String testName){
        this.appPath = appPath;
        this.appName = appName;
        this.testName = testName;
        frameworkLogger.debug("Test name: " + testName);
    }

    public TestDriver(String appPath, String appName){
        this.appPath = appPath;
        this.appName = appName;
        this.testName = "";
        frameworkLogger.debug("Test name: " + testName);
    }

    public void setCurrentTestName(String name){testName = name;
    frameworkLogger.debug("Test name: "+testName);
    }


    public Region getWindowRegion(){
        return mainWindowControl.getRegion();
    }

    public MainWindowControl mainWindowControl(){
        if (mainWindowControl == null){
            mainWindowControl = new MainWindowControl(this, appPath, appName);
        }
        return mainWindowControl;
    }

    public MenuControl menuControl(){
        return mainWindowControl.menuControl();
    }

    public EditingAreaControl editingAreaControl(){
        return mainWindowControl.editingAreaControl();
    }


    public ShortcutControl shortcutControl() {
        mainWindowControl();
       if (shortcutControl == null){
           shortcutControl = new ShortcutControl(this);
       }
        return shortcutControl;
    }


    public void user_waits(int seconds) throws InterruptedException {
        user_waits((double) seconds);
    }

    public void user_waits(double seconds) throws InterruptedException {
        mainWindowControl.getRegion().wait(seconds);

    }

    public void clearClipboard() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard content = toolkit.getSystemClipboard();
        content.setContents(new StringSelection(""), null);

    }

    public String getClipboardContent() {
        return App.getClipboard();
    }

    public void createScreenshot(Region region){
        BufferedImage image = getWindowRegion().getScreen().capture(region).getImage();
        try {
            ImageIO.write(image, "png", new File(getScreenshotPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createScreenshot(){
        BufferedImage image = getWindowRegion().getScreen().capture().getImage();;
        try {
            ImageIO.write(image, "png", new File(getScreenshotPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getScreenshotPath(){
        String screenshotPath = screenshotDir+testName+".png";
        DSL.dslLogger.info("(screenshot:" + screenshotPath+ ")");
        return screenshotPath;
    }


}
