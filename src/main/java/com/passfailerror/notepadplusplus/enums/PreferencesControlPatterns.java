package com.passfailerror.notepadplusplus.enums;


public enum PreferencesControlPatterns {

    GENERAL("general"),
    GENERAL2("general2");

    String fileName;
    private PreferencesControlPatterns(String fileName){
        this.fileName = fileName;
    }


    public String getFilename(){return fileName;}

}
