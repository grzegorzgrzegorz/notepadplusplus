package com.passfailerror.notepadplusplus.enums;

public enum FileDropdownControlPatterns {

    NEW_TAB("new_tab"),
    EXIT("exit"),
    RIGHT_BORDER("right_border");

    String fileName;
    private FileDropdownControlPatterns(String fileName){
        this.fileName = fileName;
    }

    public String getFilename(){return fileName;}

}
