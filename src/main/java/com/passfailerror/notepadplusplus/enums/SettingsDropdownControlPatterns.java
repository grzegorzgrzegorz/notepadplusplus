package com.passfailerror.notepadplusplus.enums;

public enum SettingsDropdownControlPatterns {

    PREFERENCES("preferences"),
    EDIT_POPUP_CONTEXT_MENU("edit_popup_context_menu");

    String fileName;
    private SettingsDropdownControlPatterns(String fileName){
        this.fileName = fileName;
    }

    public String getFilename(){return fileName;}

}
