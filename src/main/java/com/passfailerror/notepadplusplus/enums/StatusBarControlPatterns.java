package com.passfailerror.notepadplusplus.enums;

public enum StatusBarControlPatterns {
   //=========
    DEFAULT_LANGUAGE_C_TEXT("c_source_file", "C source file"),
    DEFAULT_LANGUAGE_NORMAL_TEXT("normal_text_file", "Normal text file"),
    DEFAULT_LANGUAGE_PHP_TEXT("php_file", "PHP Hypertext Preprocessor file"),
    DEFAULT_LANGUAGE_CPLUSPLUS_TEXT("cplusplus_source_file", "C++ source file"),
    DEFAULT_LANGUAGE_CSHARP_TEXT("csharp_source_file", "C# source file"),
    //=========
    FORMAT_UNIX_TEXT("unix_format", "Unix (LF)"),
    FORMAT_WINDOWS_TEXT("windows_format", "Windows (CR LF)"),
    FORMAT_MAC_TEXT("macintosh_format", "Macintosh (CR)"),
    //=========
    ENCODING_ANSI_TEXT("ansi_encoding", "ANSI"),
    ENCODING_UTF8_TEXT("utf8_encoding", "UTF-8"),
    ENCODING_UTF8_WITHBOM_TEXT("utf8_withbom_encoding", "UTF-8-BOM"),
    ENCODING_UCS2_BIGENDIANWITHBOM_TEXT("ucs2_bigendian_withbom_encoding", "UCS-2 BE BOM"),
    ENCODING_UCS2_LITTLEENDIANWITHBOM_TEXT("ucs2_littleendian_withbom_encoding", "UCS-2 LE BOM");



    String fileName;
    String labelName;

    private StatusBarControlPatterns(String fileName, String labelName) {
        this.fileName = fileName;
        this.labelName = labelName;
    }

    public String getFilename(){return fileName;}

    public String getLabelname(){return labelName;}

}
