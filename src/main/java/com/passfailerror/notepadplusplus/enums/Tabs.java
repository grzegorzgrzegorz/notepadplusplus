package com.passfailerror.notepadplusplus.enums;


public enum Tabs {

    new1("new1", "new1_inactive", 1),
    new2("new2", "new2_inactive", 2),
    new3("new3", "new3_inactive", 3);

    String fileName;
    String inactiveFileName;
    int tabNo;

    private Tabs(String fileName, String inactiveFileName, int tabNo){
        this.fileName = fileName;
        this.inactiveFileName = inactiveFileName;
        this.tabNo = tabNo;
    }

    public String getFilename(){return fileName;}

    public String getInactiveFilename(){return inactiveFileName;}


    public int getTabNo(){return tabNo;}

    public Tabs getTabByTabNo(int tabNo){
        switch (tabNo){
            case 1:
                return new1;
            case 2:
                return new2;
            case 3:
                return new3;
            default:
                throw new RuntimeException("unsupported tabNo: "+tabNo);
        }
    }


}
