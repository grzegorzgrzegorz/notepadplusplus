package com.passfailerror.notepadplusplus.enums;


public enum NewDocumentPreferencesControlPatterns {

    DEFAULT_LANGUAGE_LABEL("default_language_label",null,null),
    //================
    FORMAT_WINDOWS("windows_format",null,null),
    FORMAT_UNIX("unix_format",null,null),
    FORMAT_MAC("macintosh_format",null,null),
    //================
    DEFAULT_LANGUAGE_NORMAL(null,"n",2),
    DEFAULT_LANGUAGE_PHP(null,"p",1),
    DEFAULT_LANGUAGE_C(null,"c",1),
    DEFAULT_LANGUAGE_CPLUSPLUS(null,"c",2),
    DEFAULT_LANGUAGE_CSHARP(null,"c",3),
    //================
    ENCODING_ANSI("encoding_ansi",null,null),
    ENCODING_UTF8("encoding_utf8",null,null),
    ENCODING_UTF8_WITHBOM("encoding_utf8_with_bom",null,null),
    ENCODING_UCS2_BIGENDIANWITHBOM("encoding_bigendian",null,null),
    ENCODING_UCS2_LITTLEENDIANWITHBOM("encoding_littleendian",null,null),
    ENCODING_WINDOWS1250(null,"w",10),
    ENCODING_ISO88591(null,"i",1),
    ENCODING_OEMUS(null,"o",1),
    ENCODING_BIG5(null,"b",1),
    ENCODING_MACINTOSH(null,"m",1),
    //===============
    BUTTON_CLOSE("button_close",null,null);


        String fileName;
        String key;
        Integer repetitions;

        private NewDocumentPreferencesControlPatterns (String fileName, String key, Integer repetitions){
            this.fileName = fileName;
            this.key = key;
            this.repetitions = repetitions;
        }

        public String getFilename(){return fileName;}
        public String getKey(){return key;}
        public Integer getRepetitions(){return repetitions;}


}
