package com.passfailerror.notepadplusplus.enums;


public enum MenuControlPatterns {

    FILE("file"),
    SETTINGS("settings");

    String fileName;
    private MenuControlPatterns(String fileName){
        this.fileName = fileName;
    }


    public String getFilename(){return fileName;}

}
