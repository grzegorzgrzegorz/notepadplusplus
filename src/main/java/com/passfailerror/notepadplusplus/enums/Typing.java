package com.passfailerror.notepadplusplus.enums;

import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public enum Typing {

    ENTER(new String[]{Key.ENTER}),
    DELETE(new String[]{Key.DELETE}),
    CTRL_W(new String[]{"w", String.valueOf(KeyModifier.CTRL)}),
    CTRL_N(new String[]{"n", String.valueOf(KeyModifier.CTRL)}),
    CTRL_C(new String[]{"c", String.valueOf(KeyModifier.CTRL)}),
    CTRL_A(new String[]{"a", String.valueOf(KeyModifier.CTRL)}),
    ALT_F4(new String[]{Key.F4, String.valueOf(KeyModifier.ALT)}),
    RIGHT_ARROW(new String[]{Key.RIGHT}),
    CTRL_ALT_R(new String[]{"r", String.valueOf(KeyModifier.CTRL), String.valueOf(KeyModifier.ALT)}),
    CTRL_ALT_L(new String[]{"l", String.valueOf(KeyModifier.CTRL), String.valueOf(KeyModifier.ALT)}),
    PAGE_UP(new String[]{Key.PAGE_UP});

    List<String> keys;
    private Typing(String[] keys){
        this.keys = Arrays.asList(keys);
    }


    public List<String> getKeys(){return keys;}

}
