package com.passfailerror.notepadplusplus;

import org.sikuli.script.Region;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class HL_Logger {

	public static Logger logger = null;

	public static void getLogger() {
		logger = LogManager.getLogger(HL_Logger.class.getName());
	}

	private static int get_logging_level() {
		int current_level = logger.getLevel().intLevel();
		return current_level;
	}

	public static void info(Region name) {
		if (get_logging_level() >= 400) {
			name.highlight(1);
		}
	}

	public static void debug(Region name) {
		if (get_logging_level() >= 500) {
			name.highlight(1);
		}
	}

}
